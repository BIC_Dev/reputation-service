# Reputation Service

A micro service to manage a users Discord reputation score.

## How it works
Reputation can be gained and lost through a number of actions done by the user or by other users. Each positive action will increase a user's reputation by a small amount. Each negative action will decrease a user's reputation by an even smaller amount. The majority of reputation gain will be at the behest of others. Your reputation isn't how you see yourself, but how others see you. There will also be daily limits on reputation gain and loss to prevent abuse of the system.

### __Gaining Reputation__
Some ways to gain reputation are:

1. Another user reacts positively to your post
1. Sentiment analysis of your posts is positive
1. Sentiment analysis of posts you are tagged in is positive
1. Frequency of posts (daily reputation gain)

### __Losing Reputation__
Some ways to lose reputation are:

1. Another user reacts negatively to your post
1. Sentiment analysis of your posts is negative
1. Sentiment analysis of posts you are tagged in is negative

## Routes

### __GET Reputation for user (all guilds)__
Get the reputation for all guilds and global reputation score

#### Request
`GET /reputation-service/reputation/{user_id}`

### __GET Reputation for user (specific guild)__
Get the reputation for a specific guild

#### Request
`GET /reputation-service/reputation/{user_id}/{guild_id}`

### __PUT Reputation for user on guild__
Updates the reputation for a user on a specific guild

#### Request
`PUT /reputation-service/reputation`

#### Body
```json
{
    "action": "message",
    "actor_id": 280812467775471627,
    "guild_id": 626094990984216586,
    "message": "Thanks for the help @BIC",
    "message_id": 735190162887409695,
    "tagged_users": [268238111262113792],
}
```

### __GET Points for user (all guilds)__
Get the current points for all guilds and global points

#### Request
`GET /reputation-service/points/{user_id}`

### __GET Points for user (specific guild)__
Gets the current points for a specific guild and global points

#### Request
`GET /reputation-service/points/{user_id}/{guild_id}`

### __PUT Points for user on guild__
Updates the points for a user on a specific guild

#### Request
`PUT /reputation-service/points`

#### Body
```json
{
    "action": "reward|purchase|admin|reputation",
    "description": "User purchased tickets",
    "user_id": 280812467775471627,
    "guild_id": 626094990984216586,
    "admin_user_id": 268238111262113792,
    "points": -125,
}
```
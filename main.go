package main

import (
	"fmt"
	"log"
	"os"
	"reflect"
	"strconv"

	"github.com/cdipaolo/sentiment"
	"gitlab.com/BIC_Dev/reputation-service/controllers"
	"gitlab.com/BIC_Dev/reputation-service/models"
	"gitlab.com/BIC_Dev/reputation-service/routes"
	"gitlab.com/BIC_Dev/reputation-service/utils"
	"gitlab.com/BIC_Dev/reputation-service/utils/db"
)

// Service information for the service
type Service struct {
	Config *utils.Config
	Log    *utils.Log
}

var service *Service

func main() {
	env := os.Getenv("ENVIRONMENT")
	logLevel, err := strconv.Atoi(os.Getenv("LOG_LEVEL"))

	if err != nil {
		log.Fatal(err.Error())
	}

	service = &Service{
		Config: utils.GetConfig(env),
		Log:    utils.InitLog(logLevel),
	}

	if os.Getenv("MIGRATE") == "TRUE" {
		migrationErrors := service.migrateTables(models.ReputationMessageEvent{}, models.ReputationTagEvent{}, models.UserReputation{}, models.PointReputationEvent{}, models.PointAdminEvent{}, models.PointPurchaseEvent{}, models.PointRewardEvent{}, models.PointRefundEvent{}, models.UserPoints{})

		if migrationErrors != nil {
			service.Log.Log(migrationErrors, service.Log.LogFatal)
		}
	}

	model, sentErr := sentiment.Restore()

	if sentErr != nil {
		service.Log.Log(fmt.Sprintf("ERROR: Could not restore sentiment model: %s", sentErr.Error()), service.Log.LogFatal)
	}

	controller := controllers.Controller{
		Config:         service.Config,
		Log:            service.Log,
		ServiceToken:   os.Getenv("SERVICE_TOKEN"),
		SentimentModel: &model,
	}

	router := routes.GetRouter()
	routes.AddRoutes(router, &controller)

	service.Log.Log("SUCCESS: Set up router", service.Log.LogInformation)

	service.Log.Log("PROCESS: Starting MUX listener", service.Log.LogInformation)
	routes.StartListener(router, os.Getenv("LISTENER_PORT"))
}

func (s *Service) migrateTables(tables ...interface{}) []*utils.ModelError {
	s.Log.Log("Migrating tables", s.Log.LogInformation)
	var migrationErrors []*utils.ModelError

	dbStruct, dbErr := db.GetDB(s.Config)

	if dbErr != nil {
		s.Log.Log(fmt.Sprintf("%s: %s", dbErr.GetMessage(), dbErr.Error()), s.Log.LogFatal)
	}

	defer dbStruct.GetDB().Close()

	for _, table := range tables {
		migrationError := dbStruct.Migrate(table)

		if migrationError != nil {
			migrationErrors = append(migrationErrors, migrationError)
		}

		if t := reflect.TypeOf(table); t.Kind() == reflect.Ptr {
			s.Log.Log(fmt.Sprintf("Migrated table: *%s", t.Elem().Name()), s.Log.LogInformation)
		} else {
			s.Log.Log(fmt.Sprintf("Migrated table: %s", t.Name()), s.Log.LogInformation)
		}
	}

	if len(migrationErrors) > 0 {
		return migrationErrors
	}

	return nil
}

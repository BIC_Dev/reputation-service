package services

import (
	"github.com/cdipaolo/sentiment"
)

// Sentiment struct
type Sentiment struct {
	Model *sentiment.Models
}

// GetSentiment gets the sentiment for a message
// 1 = good | 0 = bad
func (s *Sentiment) GetSentiment(message string) int {
	analysis := s.Model.SentimentAnalysis(message, sentiment.English)
	return int(analysis.Score)
}

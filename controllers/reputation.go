package controllers

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"gitlab.com/BIC_Dev/reputation-service/models"
	"gitlab.com/BIC_Dev/reputation-service/services"
	"gitlab.com/BIC_Dev/reputation-service/utils"
	"gitlab.com/BIC_Dev/reputation-service/utils/db"
	"gitlab.com/BIC_Dev/reputation-service/viewmodels"
)

// GetAllReputation responds with the reputation for a user across all guilds
func (c *Controller) GetAllReputation(w http.ResponseWriter, r *http.Request) {
	if r.Header.Get("Service-Token") != c.ServiceToken {
		c.Log.Log("BLOCKED: Invalid service token", c.Log.LogMedium)
		errorResponse := viewmodels.NewErrorResponse(http.StatusUnauthorized, "Invalid service token", "Invalid service token")
		SendJSONResponse(w, errorResponse, errorResponse.StatusCode)
		return
	}

	vars := mux.Vars(r)
	userID := vars["user_id"]

	dbInterface, dbErr := db.GetDB(c.Config)

	if dbErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: %s: %s", dbErr.GetMessage(), dbErr.Error()), c.Log.LogHigh)
		errorResponse := viewmodels.NewErrorResponse(http.StatusBadRequest, dbErr.Error(), dbErr.GetMessage())
		SendJSONResponse(w, errorResponse, errorResponse.StatusCode)
		return
	}

	defer dbInterface.GetDB().Close()

	intUserID, convErr := strconv.ParseInt(userID, 10, 64)

	if convErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: Unable to convert user_id to int: %s", convErr.Error()), c.Log.LogLow)
		errorResponse := viewmodels.NewErrorResponse(http.StatusBadRequest, convErr.Error(), "Unable to convert user_id to int")
		SendJSONResponse(w, errorResponse, errorResponse.StatusCode)
		return
	}

	userReputation := models.UserReputation{
		UserID: intUserID,
	}

	reputations, getErr := userReputation.GetAll(dbInterface)

	if getErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: %s: %s", getErr.GetMessage(), getErr.Error()), c.Log.LogMedium)
		errorResponse := viewmodels.NewErrorResponse(http.StatusBadRequest, getErr.Error(), getErr.GetMessage())
		SendJSONResponse(w, errorResponse, errorResponse.StatusCode)
		return
	}

	if len(reputations) == 0 {
		c.Log.Log("ERROR: Could not find reputation for user", c.Log.LogInformation)
		errorResponse := viewmodels.NewErrorResponse(http.StatusNotFound, "No user found", "Could not find reputations for user")
		SendJSONResponse(w, errorResponse, errorResponse.StatusCode)
		return
	}

	response := viewmodels.AllGuildUserReputationResponse{
		Reputations: reputations,
	}

	SendJSONResponse(w, response, http.StatusOK)
}

// GetReputation responds with the reputation for a user on one guild
func (c *Controller) GetReputation(w http.ResponseWriter, r *http.Request) {
	if r.Header.Get("Service-Token") != c.ServiceToken {
		c.Log.Log("BLOCKED: Invalid service token", c.Log.LogMedium)
		errorResponse := viewmodels.NewErrorResponse(http.StatusUnauthorized, "Invalid service token", "Invalid service token")
		SendJSONResponse(w, errorResponse, errorResponse.StatusCode)
		return
	}

	vars := mux.Vars(r)
	userID := vars["user_id"]
	guildID := vars["guild_id"]

	dbInterface, dbErr := db.GetDB(c.Config)

	if dbErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: %s: %s", dbErr.GetMessage(), dbErr.Error()), c.Log.LogHigh)
		errorResponse := viewmodels.NewErrorResponse(http.StatusBadRequest, dbErr.Error(), dbErr.GetMessage())
		SendJSONResponse(w, errorResponse, errorResponse.StatusCode)
		return
	}

	defer dbInterface.GetDB().Close()

	intUserID, convUserErr := strconv.ParseInt(userID, 10, 64)
	intGuild, convGuildErr := strconv.ParseInt(guildID, 10, 64)

	if convUserErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: Unable to convert user_id to int: %s", convUserErr.Error()), c.Log.LogLow)
		errorResponse := viewmodels.NewErrorResponse(http.StatusBadRequest, convUserErr.Error(), "Unable to convert user_id to int")
		SendJSONResponse(w, errorResponse, errorResponse.StatusCode)
		return
	}

	if convGuildErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: Unable to convert guild_id to int: %s", convGuildErr.Error()), c.Log.LogLow)
		errorResponse := viewmodels.NewErrorResponse(http.StatusBadRequest, convGuildErr.Error(), "Unable to convert guild_id to int")
		SendJSONResponse(w, errorResponse, errorResponse.StatusCode)
		return
	}

	userReputation := models.UserReputation{
		UserID:  intUserID,
		GuildID: intGuild,
	}

	reputation, getErr := userReputation.GetByGuild(dbInterface)

	if getErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: %s: %s", getErr.GetMessage(), getErr.Error()), c.Log.LogMedium)
		errorResponse := viewmodels.NewErrorResponse(http.StatusBadRequest, getErr.Error(), getErr.GetMessage())
		SendJSONResponse(w, errorResponse, errorResponse.StatusCode)
		return
	}

	if reputation == nil {
		c.Log.Log("ERROR: Could not find reputation for user", c.Log.LogInformation)
		errorResponse := viewmodels.NewErrorResponse(http.StatusNotFound, "No user found", "Could not find reputation for user")
		SendJSONResponse(w, errorResponse, errorResponse.StatusCode)
		return
	}

	response := viewmodels.GuildUserReputationResponse{
		Reputation: reputation,
	}

	SendJSONResponse(w, response, http.StatusOK)
}

// UpdateReputation updates reputation for user(s)
func (c *Controller) UpdateReputation(w http.ResponseWriter, r *http.Request) {
	if r.Header.Get("Service-Token") != c.ServiceToken {
		c.Log.Log("BLOCKED: Invalid service token", c.Log.LogMedium)
		errorResponse := viewmodels.NewErrorResponse(http.StatusUnauthorized, "Invalid service token", "Invalid service token")
		SendJSONResponse(w, errorResponse, errorResponse.StatusCode)
		return
	}

	dbInterface, dbErr := db.GetDB(c.Config)

	if dbErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: %s: %s", dbErr.GetMessage(), dbErr.Error()), c.Log.LogHigh)
		errorResponse := viewmodels.NewErrorResponse(http.StatusBadRequest, dbErr.Error(), dbErr.GetMessage())
		SendJSONResponse(w, errorResponse, errorResponse.StatusCode)
		return
	}

	defer dbInterface.GetDB().Close()

	reqBody, err := ioutil.ReadAll(r.Body)

	if err != nil {
		errorResponse := viewmodels.NewErrorResponse(http.StatusBadRequest, err.Error(), "Could not read body of PUT Reputation request")
		SendJSONResponse(w, errorResponse, errorResponse.StatusCode)
		return
	}

	var request viewmodels.UpdateReputationRequest
	json.Unmarshal(reqBody, &request)

	c.Log.Log("PROCESS: Update reputation request validation", c.Log.LogInformation)

	validationErr := request.Validate()

	if validationErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: Update reputation request validation failed: %s", validationErr.Error()), c.Log.LogMedium)
		errorResponse := viewmodels.NewErrorResponse(http.StatusBadRequest, validationErr.Error(), validationErr.GetMessage())
		SendJSONResponse(w, errorResponse, errorResponse.StatusCode)
		return
	}

	c.Log.Log("SUCCESS: Update reputation request validation passed", c.Log.LogInformation)

	repEvent := models.ReputationMessageEvent{
		UserID:    request.ActorID,
		MessageID: request.MessageID,
	}

	prevRepEnt, getErr := repEvent.GetMessage(dbInterface)

	if getErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: Update reputation request failed to fetch previous message: %s", getErr.Error()), c.Log.LogHigh)
		errorResponse := viewmodels.NewErrorResponse(http.StatusInternalServerError, getErr.Error(), getErr.GetMessage())
		SendJSONResponse(w, errorResponse, errorResponse.StatusCode)
		return
	}

	var reputationMessageEvents []*models.ReputationMessageEvent
	var reputationTagEvents []*models.ReputationTagEvent
	var userReputations []*models.UserReputation
	var pointReputationEvents []*models.PointReputationEvent
	var userPoints []*models.UserPoints

	switch request.Action {
	case "message":
		// Save reputation event
		// Update reputation for actor
		// Update reputation for tagged members
		// IF positive give points to actor
		// IF positive give points to tagged members

		if prevRepEnt != nil {
			break
		}

		sentiment := services.Sentiment{
			Model: c.SentimentModel,
		}

		score := sentiment.GetSentiment(request.Message)

		repEvent, err := saveReputationEventMessage(dbInterface, score, &request)

		if err != nil {
			c.Log.Log(fmt.Sprintf("ERROR: Update reputation request failed to save message: %s", err.Error()), c.Log.LogHigh)
			errorResponse := viewmodels.NewErrorResponse(http.StatusInternalServerError, err.Error(), "Update reputation request failed to save message")
			SendJSONResponse(w, errorResponse, errorResponse.StatusCode)
			return
		}

		reputationMessageEvents = append(reputationMessageEvents, repEvent)

		userRep, udErr := updateUserReputation(dbInterface, score, request.ActorID, request.GuildID)

		if udErr != nil {
			c.Log.Log(fmt.Sprintf("ERROR: Update reputation request failed to update user reputation: %s", udErr.Error()), c.Log.LogHigh)
			errorResponse := viewmodels.NewErrorResponse(http.StatusInternalServerError, udErr.Error(), "Update reputation request failed to update user reputation")
			SendJSONResponse(w, errorResponse, errorResponse.StatusCode)
			return
		}

		userReputations = append(userReputations, userRep)

		if score == 1 {
			pointEvent, err := savePointsEvent(dbInterface, request.ActorID, "User posted a positive message", c.Config.Points.Message, &request)

			if err != nil {
				c.Log.Log(fmt.Sprintf("ERROR: Update reputation request failed to save points event: %s", err.Error()), c.Log.LogHigh)
				errorResponse := viewmodels.NewErrorResponse(http.StatusInternalServerError, err.Error(), "Update reputation request failed to save points event")
				SendJSONResponse(w, errorResponse, errorResponse.StatusCode)
				return
			}

			pointReputationEvents = append(pointReputationEvents, pointEvent)

			userPoint, upErr := updateUserPoints(dbInterface, request.ActorID, request.GuildID, c.Config.Points.Message)

			if upErr != nil {
				c.Log.Log(fmt.Sprintf("ERROR: Update reputation request failed to give points to user: %s", upErr.Error()), c.Log.LogHigh)
				errorResponse := viewmodels.NewErrorResponse(http.StatusInternalServerError, upErr.Error(), "Update reputation request failed to give points to user")
				SendJSONResponse(w, errorResponse, errorResponse.StatusCode)
				return
			}

			userPoints = append(userPoints, userPoint)
		}

		for _, taggedUserID := range request.TaggedUsers {
			if taggedUserID == request.ActorID {
				continue
			}

			prevTagEvent, err := getReputationEventTagged(dbInterface, taggedUserID, repEvent.ID)

			if err != nil {
				c.Log.Log(fmt.Sprintf("ERROR: Update reputation request failed to get tagged event: %s", err.Error()), c.Log.LogHigh)
				errorResponse := viewmodels.NewErrorResponse(http.StatusInternalServerError, err.Error(), "Update reputation request failed to get tagged event")
				SendJSONResponse(w, errorResponse, errorResponse.StatusCode)
				return
			}

			if prevTagEvent != nil {
				continue
			}

			repEventTag, tagErr := saveReputationEventTagged(dbInterface, score, taggedUserID, request.GuildID, repEvent.ID)

			if tagErr != nil {
				c.Log.Log(fmt.Sprintf("ERROR: Update reputation request failed to save tagged event: %s", tagErr.Error()), c.Log.LogHigh)
				errorResponse := viewmodels.NewErrorResponse(http.StatusInternalServerError, tagErr.Error(), "Update reputation request failed to save tagged event")
				SendJSONResponse(w, errorResponse, errorResponse.StatusCode)
				return
			}

			reputationTagEvents = append(reputationTagEvents, repEventTag)

			userRep, udErr := updateUserReputation(dbInterface, score, taggedUserID, request.GuildID)

			if udErr != nil {
				c.Log.Log(fmt.Sprintf("ERROR: Update reputation request failed to update user reputation: %s", udErr.Error()), c.Log.LogHigh)
				errorResponse := viewmodels.NewErrorResponse(http.StatusInternalServerError, udErr.Error(), "Update reputation request failed to update user reputation")
				SendJSONResponse(w, errorResponse, errorResponse.StatusCode)
				return
			}

			userReputations = append(userReputations, userRep)

			if score == 1 {
				pointEvent, err := savePointsEvent(dbInterface, taggedUserID, "User tagged in a positive message", c.Config.Points.Tag, &request)

				if err != nil {
					c.Log.Log(fmt.Sprintf("ERROR: Update reputation request failed to give points to tagged user: %s", err.Error()), c.Log.LogHigh)
					errorResponse := viewmodels.NewErrorResponse(http.StatusInternalServerError, err.Error(), "Update reputation request failed to give points to tagged user")
					SendJSONResponse(w, errorResponse, errorResponse.StatusCode)
					return
				}

				pointReputationEvents = append(pointReputationEvents, pointEvent)

				userPoint, upErr := updateUserPoints(dbInterface, taggedUserID, request.GuildID, c.Config.Points.Tag)

				if upErr != nil {
					c.Log.Log(fmt.Sprintf("ERROR: Update reputation request failed to give points to user: %s", upErr.Error()), c.Log.LogHigh)
					errorResponse := viewmodels.NewErrorResponse(http.StatusInternalServerError, upErr.Error(), "Update reputation request failed to give points to user")
					SendJSONResponse(w, errorResponse, errorResponse.StatusCode)
					return
				}

				userPoints = append(userPoints, userPoint)

			}
		}
	default:
		c.Log.Log("Unknown action type for update reputation", c.Log.LogInformation)
		errorResponse := viewmodels.NewErrorResponse(http.StatusBadRequest, "Unknown action type", fmt.Sprintf("Update reputation request failed due to unknown action type: %s", request.Action))
		SendJSONResponse(w, errorResponse, errorResponse.StatusCode)
		return
	}

	response := viewmodels.UpdateReputationResponse{
		Message:                 "Successfully updated reputations and points",
		ReputationMessageEvents: reputationMessageEvents,
		ReputationTagEvents:     reputationTagEvents,
		UserReputations:         userReputations,
		PointReputationEvents:   pointReputationEvents,
		UserPoints:              userPoints,
	}

	SendJSONResponse(w, response, http.StatusOK)
	return
}

func saveReputationEventMessage(dbInterface db.Interface, score int, request *viewmodels.UpdateReputationRequest) (*models.ReputationMessageEvent, *utils.ModelError) {
	repEvent := models.ReputationMessageEvent{
		UserID:    request.ActorID,
		GuildID:   request.GuildID,
		MessageID: request.MessageID,
		Message:   request.Message,
		Sentiment: int(score),
	}

	err := repEvent.Create(dbInterface)

	if err != nil {
		return nil, err
	}

	return &repEvent, nil
}

func saveReputationEventTagged(dbInterface db.Interface, score int, taggedUserID int64, guildID int64, reputationMessageEventID uint) (*models.ReputationTagEvent, *utils.ModelError) {
	repEvent := models.ReputationTagEvent{
		ReputationMessageEvent: reputationMessageEventID,
		UserID:                 taggedUserID,
		GuildID:                guildID,
		Sentiment:              int(score),
	}

	err := repEvent.Create(dbInterface)

	if err != nil {
		return nil, err
	}

	return &repEvent, nil
}

func getReputationEventTagged(dbInterface db.Interface, taggedUserID int64, reputationMessageEventID uint) (*models.ReputationTagEvent, *utils.ModelError) {
	repEvent := models.ReputationTagEvent{
		UserID:                 taggedUserID,
		ReputationMessageEvent: reputationMessageEventID,
	}

	prevRepEvent, err := repEvent.GetTagByUserAndMessage(dbInterface)

	if err != nil {
		return nil, err
	}

	return prevRepEvent, nil
}

func updateUserReputation(dbInterface db.Interface, score int, userID int64, guildID int64) (*models.UserReputation, *utils.ModelError) {
	userRep := &models.UserReputation{
		UserID:  userID,
		GuildID: guildID,
	}

	var err *utils.ModelError

	if score == 0 {
		err = userRep.NegativeEvent(dbInterface)
	} else if score == 1 {
		err = userRep.PositiveEvent(dbInterface)
	}

	if err != nil {
		return nil, err
	}

	return userRep, nil
}

func savePointsEvent(dbInterface db.Interface, userID int64, reason string, points int, request *viewmodels.UpdateReputationRequest) (*models.PointReputationEvent, *utils.ModelError) {
	pointEvent := models.PointReputationEvent{
		Reason:  reason,
		UserID:  userID,
		GuildID: request.GuildID,
		Points:  points,
	}

	err := pointEvent.Create(dbInterface)

	if err != nil {
		return nil, err
	}

	return &pointEvent, nil
}

func updateUserPoints(dbInterface db.Interface, userID int64, guildID int64, points int) (*models.UserPoints, *utils.ModelError) {
	userPoints := &models.UserPoints{
		UserID:  userID,
		GuildID: guildID,
	}

	err := userPoints.UpdatePoints(dbInterface, points)

	if err != nil {
		return nil, err
	}

	return userPoints, nil
}

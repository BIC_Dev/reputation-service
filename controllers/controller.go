package controllers

import (
	"encoding/json"
	"net/http"

	"github.com/cdipaolo/sentiment"
	"gitlab.com/BIC_Dev/reputation-service/utils"
)

// Controller struct containing DB access and Configs
type Controller struct {
	Config         *utils.Config
	Log            *utils.Log
	ServiceToken   string
	SentimentModel *sentiment.Models
}

// SendJSONResponse sends a response to the client
func SendJSONResponse(w http.ResponseWriter, response interface{}, statusCode int) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(statusCode)
	json.NewEncoder(w).Encode(response)
}

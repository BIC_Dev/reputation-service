package controllers

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"gitlab.com/BIC_Dev/reputation-service/models"
	"gitlab.com/BIC_Dev/reputation-service/utils"
	"gitlab.com/BIC_Dev/reputation-service/utils/db"
	"gitlab.com/BIC_Dev/reputation-service/viewmodels"
)

// GetAllPoints responds with the points for a user across all guilds
func (c *Controller) GetAllPoints(w http.ResponseWriter, r *http.Request) {
	if r.Header.Get("Service-Token") != c.ServiceToken {
		c.Log.Log("BLOCKED: Invalid service token", c.Log.LogMedium)
		errorResponse := viewmodels.NewErrorResponse(http.StatusUnauthorized, "Invalid service token", "Invalid service token")
		SendJSONResponse(w, errorResponse, errorResponse.StatusCode)
		return
	}

	vars := mux.Vars(r)
	userID := vars["user_id"]

	dbInterface, dbErr := db.GetDB(c.Config)

	if dbErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: %s: %s", dbErr.GetMessage(), dbErr.Error()), c.Log.LogHigh)
		errorResponse := viewmodels.NewErrorResponse(http.StatusBadRequest, dbErr.Error(), dbErr.GetMessage())
		SendJSONResponse(w, errorResponse, errorResponse.StatusCode)
		return
	}

	defer dbInterface.GetDB().Close()

	intUserID, convErr := strconv.ParseInt(userID, 10, 64)

	if convErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: Unable to convert user_id to int: %s", convErr.Error()), c.Log.LogLow)
		errorResponse := viewmodels.NewErrorResponse(http.StatusBadRequest, convErr.Error(), "Unable to convert user_id to int")
		SendJSONResponse(w, errorResponse, errorResponse.StatusCode)
		return
	}

	userPoints := models.UserPoints{
		UserID: intUserID,
	}

	points, getErr := userPoints.GetAll(dbInterface)

	if getErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: %s: %s", getErr.GetMessage(), getErr.Error()), c.Log.LogMedium)
		errorResponse := viewmodels.NewErrorResponse(http.StatusBadRequest, getErr.Error(), getErr.GetMessage())
		SendJSONResponse(w, errorResponse, errorResponse.StatusCode)
		return
	}

	if len(points) == 0 {
		c.Log.Log("ERROR: Could not find all points for user", c.Log.LogInformation)
		errorResponse := viewmodels.NewErrorResponse(http.StatusNotFound, "No user found", "Could not find all points for user")
		SendJSONResponse(w, errorResponse, errorResponse.StatusCode)
		return
	}

	response := viewmodels.AllGuildUserPointsResponse{
		AllPoints: points,
	}

	SendJSONResponse(w, response, http.StatusOK)
}

// GetPoints responds with the points for a user on one guild
func (c *Controller) GetPoints(w http.ResponseWriter, r *http.Request) {
	if r.Header.Get("Service-Token") != c.ServiceToken {
		c.Log.Log("BLOCKED: Invalid service token", c.Log.LogMedium)
		errorResponse := viewmodels.NewErrorResponse(http.StatusUnauthorized, "Invalid service token", "Invalid service token")
		SendJSONResponse(w, errorResponse, errorResponse.StatusCode)
		return
	}

	vars := mux.Vars(r)
	userID := vars["user_id"]
	guildID := vars["guild_id"]

	dbInterface, dbErr := db.GetDB(c.Config)

	if dbErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: %s: %s", dbErr.GetMessage(), dbErr.Error()), c.Log.LogHigh)
		errorResponse := viewmodels.NewErrorResponse(http.StatusBadRequest, dbErr.Error(), dbErr.GetMessage())
		SendJSONResponse(w, errorResponse, errorResponse.StatusCode)
		return
	}

	defer dbInterface.GetDB().Close()

	intUserID, convUserErr := strconv.ParseInt(userID, 10, 64)
	intGuild, convGuildErr := strconv.ParseInt(guildID, 10, 64)

	if convUserErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: Unable to convert user_id to int: %s", convUserErr.Error()), c.Log.LogLow)
		errorResponse := viewmodels.NewErrorResponse(http.StatusBadRequest, convUserErr.Error(), "Unable to convert user_id to int")
		SendJSONResponse(w, errorResponse, errorResponse.StatusCode)
		return
	}

	if convGuildErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: Unable to convert guild_id to int: %s", convGuildErr.Error()), c.Log.LogLow)
		errorResponse := viewmodels.NewErrorResponse(http.StatusBadRequest, convGuildErr.Error(), "Unable to convert guild_id to int")
		SendJSONResponse(w, errorResponse, errorResponse.StatusCode)
		return
	}

	userPoints := models.UserPoints{
		UserID:  intUserID,
		GuildID: intGuild,
	}

	points, getErr := userPoints.GetByGuild(dbInterface)

	if getErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: %s: %s", getErr.GetMessage(), getErr.Error()), c.Log.LogMedium)
		errorResponse := viewmodels.NewErrorResponse(http.StatusBadRequest, getErr.Error(), getErr.GetMessage())
		SendJSONResponse(w, errorResponse, errorResponse.StatusCode)
		return
	}

	if points == nil {
		c.Log.Log("ERROR: Could not find points for user", c.Log.LogInformation)
		errorResponse := viewmodels.NewErrorResponse(http.StatusNotFound, "No user found", "Could not find points for user")
		SendJSONResponse(w, errorResponse, errorResponse.StatusCode)
		return
	}

	response := viewmodels.GuildUserPointsResponse{
		Points: points,
	}

	SendJSONResponse(w, response, http.StatusOK)
	return
}

// UpdatePoints updates points for a user
func (c *Controller) UpdatePoints(w http.ResponseWriter, r *http.Request) {
	if r.Header.Get("Service-Token") != c.ServiceToken {
		c.Log.Log("BLOCKED: Invalid service token", c.Log.LogMedium)
		errorResponse := viewmodels.NewErrorResponse(http.StatusUnauthorized, "Invalid service token", "Invalid service token")
		SendJSONResponse(w, errorResponse, errorResponse.StatusCode)
		return
	}

	dbInterface, dbErr := db.GetDB(c.Config)

	if dbErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: %s: %s", dbErr.GetMessage(), dbErr.Error()), c.Log.LogHigh)
		errorResponse := viewmodels.NewErrorResponse(http.StatusBadRequest, dbErr.Error(), dbErr.GetMessage())
		SendJSONResponse(w, errorResponse, errorResponse.StatusCode)
		return
	}

	defer dbInterface.GetDB().Close()

	reqBody, err := ioutil.ReadAll(r.Body)

	if err != nil {
		errorResponse := viewmodels.NewErrorResponse(http.StatusBadRequest, err.Error(), "Could not read body of PUT Points request")
		SendJSONResponse(w, errorResponse, errorResponse.StatusCode)
		return
	}

	var request viewmodels.UpdatePointsRequest
	json.Unmarshal(reqBody, &request)

	c.Log.Log("PROCESS: Update points request validation", c.Log.LogInformation)

	validationErr := request.Validate()

	if validationErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: Update points request validation failed: %s", validationErr.Error()), c.Log.LogMedium)
		errorResponse := viewmodels.NewErrorResponse(http.StatusBadRequest, validationErr.Error(), validationErr.GetMessage())
		SendJSONResponse(w, errorResponse, errorResponse.StatusCode)
		return
	}

	c.Log.Log("SUCCESS: Update points request validation passed", c.Log.LogInformation)

	var pointRewardsEvent *models.PointRewardEvent = nil
	var pointReputationEvent *models.PointReputationEvent = nil
	var pointPurchaseEvent *models.PointPurchaseEvent = nil
	var pointAdminEvent *models.PointAdminEvent = nil
	var pointRefundEvent *models.PointRefundEvent = nil

	switch request.Action {
	case "reward":
		event, err := savePointRewardsEvent(dbInterface, &request)

		if err != nil {
			c.Log.Log(fmt.Sprintf("ERROR: Update points request failed to save point rewards event: %s", err.Error()), c.Log.LogHigh)
			errorResponse := viewmodels.NewErrorResponse(http.StatusInternalServerError, err.Error(), "Update reputation request failed to save point rewards event")
			SendJSONResponse(w, errorResponse, errorResponse.StatusCode)
			return
		}

		pointRewardsEvent = event
	case "purchase":
		event, err := savePointPurchaseEvent(dbInterface, &request)

		if err != nil {
			c.Log.Log(fmt.Sprintf("ERROR: Update points request failed to save point purchase event: %s", err.Error()), c.Log.LogHigh)
			errorResponse := viewmodels.NewErrorResponse(http.StatusInternalServerError, err.Error(), "Update reputation request failed to save point purchase event")
			SendJSONResponse(w, errorResponse, errorResponse.StatusCode)
			return
		}

		pointPurchaseEvent = event
	case "admin":
		event, err := savePointAdminEvent(dbInterface, &request)

		if err != nil {
			c.Log.Log(fmt.Sprintf("ERROR: Update points request failed to save point admin event: %s", err.Error()), c.Log.LogHigh)
			errorResponse := viewmodels.NewErrorResponse(http.StatusInternalServerError, err.Error(), "Update reputation request failed to save point admin event")
			SendJSONResponse(w, errorResponse, errorResponse.StatusCode)
			return
		}

		pointAdminEvent = event
	case "reputation":
		event, err := savePointReputationEvent(dbInterface, &request)

		if err != nil {
			c.Log.Log(fmt.Sprintf("ERROR: Update points request failed to save point reputation event: %s", err.Error()), c.Log.LogHigh)
			errorResponse := viewmodels.NewErrorResponse(http.StatusInternalServerError, err.Error(), "Update reputation request failed to save point reputation event")
			SendJSONResponse(w, errorResponse, errorResponse.StatusCode)
			return
		}

		pointReputationEvent = event
	case "refund":
		event, err := savePointRefundEvent(dbInterface, &request)

		if err != nil {
			c.Log.Log(fmt.Sprintf("ERROR: Update points request failed to save point refund event: %s", err.Error()), c.Log.LogHigh)
			errorResponse := viewmodels.NewErrorResponse(http.StatusInternalServerError, err.Error(), "Update reputation request failed to save point reputation event")
			SendJSONResponse(w, errorResponse, errorResponse.StatusCode)
			return
		}

		pointRefundEvent = event
	default:
		c.Log.Log("Unknown action type for update points", c.Log.LogInformation)
		errorResponse := viewmodels.NewErrorResponse(http.StatusBadRequest, "Unknown action type", fmt.Sprintf("Update points request failed due to unknown action type: %s", request.Action))
		SendJSONResponse(w, errorResponse, errorResponse.StatusCode)
		return
	}

	userPoints, upErr := updatePoints(dbInterface, &request)

	if upErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: Update points request failed to update user points: %s", upErr.Error()), c.Log.LogHigh)
		errorResponse := viewmodels.NewErrorResponse(http.StatusInternalServerError, upErr.Error(), "Update reputation request failed to update user points")
		SendJSONResponse(w, errorResponse, errorResponse.StatusCode)
		return
	}

	response := viewmodels.UpdatePointsResponse{
		Message:              "Successfully update points for user",
		PointPurchaseEvent:   pointPurchaseEvent,
		PointRewardEvents:    pointRewardsEvent,
		PointAdminEvent:      pointAdminEvent,
		PointReputationEvent: pointReputationEvent,
		PointRefundEvent:     pointRefundEvent,
		UserPoints:           userPoints,
	}

	SendJSONResponse(w, response, http.StatusOK)
	return
}

func savePointRewardsEvent(dbInterface db.Interface, request *viewmodels.UpdatePointsRequest) (*models.PointRewardEvent, *utils.ModelError) {
	pointRewardEvent := models.PointRewardEvent{
		Reason:  request.Reason,
		UserID:  request.UserID,
		GuildID: request.GuildID,
		Points:  request.Points,
	}

	err := pointRewardEvent.Create(dbInterface)

	if err != nil {
		return nil, err
	}

	return &pointRewardEvent, nil
}

func savePointReputationEvent(dbInterface db.Interface, request *viewmodels.UpdatePointsRequest) (*models.PointReputationEvent, *utils.ModelError) {
	pointReputationEvent := models.PointReputationEvent{
		Reason:  request.Reason,
		UserID:  request.UserID,
		GuildID: request.GuildID,
		Points:  request.Points,
	}

	err := pointReputationEvent.Create(dbInterface)

	if err != nil {
		return nil, err
	}

	return &pointReputationEvent, nil
}

func savePointPurchaseEvent(dbInterface db.Interface, request *viewmodels.UpdatePointsRequest) (*models.PointPurchaseEvent, *utils.ModelError) {
	pointPurchaseEvent := models.PointPurchaseEvent{
		Reason:  request.Reason,
		UserID:  request.UserID,
		GuildID: request.GuildID,
		Points:  request.Points,
	}

	err := pointPurchaseEvent.Create(dbInterface)

	if err != nil {
		return nil, err
	}

	return &pointPurchaseEvent, nil
}

func savePointAdminEvent(dbInterface db.Interface, request *viewmodels.UpdatePointsRequest) (*models.PointAdminEvent, *utils.ModelError) {
	pointAdminEvent := models.PointAdminEvent{
		Reason:      request.Reason,
		UserID:      request.UserID,
		GuildID:     request.GuildID,
		AdminUserID: request.AdminUserID,
		Points:      request.Points,
	}

	err := pointAdminEvent.Create(dbInterface)

	if err != nil {
		return nil, err
	}

	return &pointAdminEvent, nil
}

func savePointRefundEvent(dbInterface db.Interface, request *viewmodels.UpdatePointsRequest) (*models.PointRefundEvent, *utils.ModelError) {
	pointRefundEvent := models.PointRefundEvent{
		Reason:  request.Reason,
		UserID:  request.UserID,
		GuildID: request.GuildID,
		Points:  request.Points,
	}

	err := pointRefundEvent.Create(dbInterface)

	if err != nil {
		return nil, err
	}

	return &pointRefundEvent, nil
}

func updatePoints(dbInterface db.Interface, request *viewmodels.UpdatePointsRequest) (*models.UserPoints, *utils.ModelError) {
	userPoints := models.UserPoints{
		UserID:  request.UserID,
		GuildID: request.GuildID,
	}

	err := userPoints.UpdatePoints(dbInterface, request.Points)

	if err != nil {
		return nil, err
	}

	return &userPoints, nil
}

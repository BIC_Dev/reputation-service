package models

import (
	"net/http"
	"time"

	"github.com/jinzhu/gorm"
	"gitlab.com/BIC_Dev/reputation-service/utils"
	"gitlab.com/BIC_Dev/reputation-service/utils/db"
)

// ReputationTagEvent struct
type ReputationTagEvent struct {
	ID                     uint       `gorm:"primary_key" json:"id"`
	ReputationMessageEvent uint       `json:"reputation_message_event"`
	UserID                 int64      `json:"user_id"`
	GuildID                int64      `json:"guild_id"`
	Sentiment              int        `json:"sentiment"`
	CreatedAt              time.Time  `json:"-"`
	UpdatedAt              time.Time  `json:"-"`
	DeletedAt              *time.Time `sql:"index" json:"-"`
}

// TableName for the db
func (ReputationTagEvent) TableName() string {
	return "reputation_tag_events"
}

// Create adds a record to DB
func (t *ReputationTagEvent) Create(DBStruct db.Interface) *utils.ModelError {
	result := DBStruct.GetDB().Create(&t)

	if result.Error != nil {
		modelError := utils.NewModelError(result.Error)
		modelError.SetMessage("Unable to create reputation tag event entry in DB")
		modelError.SetStatus(http.StatusBadRequest)
		return modelError
	}

	return nil
}

// GetTagByUserAndMessage gets the latest record for a reputation tag event for by user for message
func (t *ReputationTagEvent) GetTagByUserAndMessage(DBStruct db.Interface) (*ReputationTagEvent, *utils.ModelError) {
	var event ReputationTagEvent

	result := DBStruct.GetDB().Model(&ReputationTagEvent{}).Where("user_id = ? AND reputation_message_event = ?", t.UserID, t.ReputationMessageEvent).Last(&event)

	if gorm.IsRecordNotFoundError(result.Error) {
		return nil, nil
	}

	if result.Error != nil {
		modelError := utils.NewModelError(result.Error)
		modelError.SetMessage("DB error finding reputation tag event by user and message")
		modelError.SetStatus(http.StatusBadRequest)
		return nil, modelError
	}

	return &event, nil
}

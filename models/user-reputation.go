package models

import (
	"math"
	"net/http"
	"time"

	"github.com/jinzhu/gorm"
	"gitlab.com/BIC_Dev/reputation-service/utils"
	"gitlab.com/BIC_Dev/reputation-service/utils/db"
)

// PositiveEventModifier amount to modify reputation by
const PositiveEventModifier = 1.0

// NegativeEventModifier amount to modify reputation by
const NegativeEventModifier = -.25

// NeutralEventModifier amount to modify reputation by
const NeutralEventModifier = 0.0

// UserReputation struct
type UserReputation struct {
	ID               uint       `gorm:"primary_key" json:"id"`
	UserID           int64      `json:"user_id"`
	GuildID          int64      `json:"guild_id"`
	PositiveEvents   int        `json:"positive_events"`
	NegativeEvents   int        `json:"negative_events"`
	NeutralEvents    int        `json:"neutral_events"`
	ReputationPoints float64    `json:"reputation_points"`
	ReputationScore  float64    `json:"reputation_score"`
	CreatedAt        time.Time  `json:"-"`
	UpdatedAt        time.Time  `json:"-"`
	DeletedAt        *time.Time `sql:"index" json:"-"`
}

// TableName for the db
func (UserReputation) TableName() string {
	return "user_reputation"
}

// Create adds a record to DB
func (t *UserReputation) Create(DBStruct db.Interface) *utils.ModelError {
	result := DBStruct.GetDB().Create(&t)

	if result.Error != nil {
		modelError := utils.NewModelError(result.Error)
		modelError.SetMessage("Unable to create user reputation entry in DB")
		modelError.SetStatus(http.StatusBadRequest)
		return modelError
	}

	return nil
}

// GetAll gets all reputation sources for a user
func (t *UserReputation) GetAll(DBStruct db.Interface) ([]*UserReputation, *utils.ModelError) {
	var reputation []*UserReputation

	result := DBStruct.GetDB().Model(&UserReputation{}).Where("user_id = ?", t.UserID).Find(&reputation)

	if gorm.IsRecordNotFoundError(result.Error) {
		return nil, nil
	}

	if result.Error != nil {
		modelError := utils.NewModelError(result.Error)
		modelError.SetMessage("DB error finding user reputation for all guilds")
		modelError.SetStatus(http.StatusBadRequest)
		return nil, modelError
	}

	return reputation, nil
}

// GetByGuild gets a users reputation score by guild
func (t *UserReputation) GetByGuild(DBStruct db.Interface) (*UserReputation, *utils.ModelError) {
	var reputation UserReputation

	result := DBStruct.GetDB().Model(&ReputationMessageEvent{}).Where("user_id = ? AND guild_id = ?", t.UserID, t.GuildID).Last(&reputation)

	if gorm.IsRecordNotFoundError(result.Error) {
		return nil, nil
	}

	if result.Error != nil {
		modelError := utils.NewModelError(result.Error)
		modelError.SetMessage("DB error finding user reputation by guild")
		modelError.SetStatus(http.StatusBadRequest)
		return nil, modelError
	}

	return &reputation, nil
}

// PositiveEvent updates a user in a guild with a positive event
func (t *UserReputation) PositiveEvent(DBStruct db.Interface) *utils.ModelError {
	getResult, getErr := t.GetByGuild(DBStruct)

	if getErr != nil {
		return getErr
	}

	if getResult == nil {
		t.PositiveEvents = 1
		t.ReputationPoints = PositiveEventModifier
		t.ReputationScore = 1.0
		createErr := t.Create(DBStruct)

		if createErr != nil {
			return createErr
		}

		return nil
	}

	result := DBStruct.GetDB().Model(&UserReputation{}).Where("user_id = ? AND guild_id = ?", t.UserID, t.GuildID).Updates(map[string]interface{}{
		"positive_events":   getResult.PositiveEvents + 1,
		"reputation_points": getResult.ReputationPoints + PositiveEventModifier,
		"reputation_score":  calculateReputationScore(getResult.ReputationPoints+PositiveEventModifier, getResult.PositiveEvents+1),
	})

	if result.Error != nil {
		modelError := utils.NewModelError(result.Error)
		modelError.SetMessage("Unable to update user reputation for positive event in DB")
		modelError.SetStatus(http.StatusBadRequest)
		return modelError
	}

	getResultAfterUpdate, getErrAfterUpdate := t.GetByGuild(DBStruct)

	if getErrAfterUpdate != nil {
		return getErr
	}

	*t = *getResultAfterUpdate

	return nil
}

// NegativeEvent updates a user in a guild with a negative event
func (t *UserReputation) NegativeEvent(DBStruct db.Interface) *utils.ModelError {
	getResult, getErr := t.GetByGuild(DBStruct)

	if getErr != nil {
		return getErr
	}

	if getResult == nil {
		t.NegativeEvents = 1
		t.ReputationPoints = NegativeEventModifier
		t.ReputationScore = 0.0
		createErr := t.Create(DBStruct)

		if createErr != nil {
			return createErr
		}

		return nil
	}

	result := DBStruct.GetDB().Model(&UserReputation{}).Where("user_id = ? AND guild_id = ?", t.UserID, t.GuildID).Updates(map[string]interface{}{
		"negative_events":   getResult.NegativeEvents + 1,
		"reputation_points": getResult.ReputationPoints + NegativeEventModifier,
		"reputation_score":  calculateReputationScore(getResult.ReputationPoints+NegativeEventModifier, getResult.PositiveEvents),
	})

	if result.Error != nil {
		modelError := utils.NewModelError(result.Error)
		modelError.SetMessage("Unable to update user reputation for negative event in DB")
		modelError.SetStatus(http.StatusBadRequest)
		return modelError
	}

	getResultAfterUpdate, getErrAfterUpdate := t.GetByGuild(DBStruct)

	if getErrAfterUpdate != nil {
		return getErr
	}

	*t = *getResultAfterUpdate

	return nil
}

// NeutralEvent updates a user in a guild with a negative event
func (t *UserReputation) NeutralEvent(DBStruct db.Interface) *utils.ModelError {
	getResult, getErr := t.GetByGuild(DBStruct)

	if getErr != nil {
		return getErr
	}

	if getResult == nil {
		t.NeutralEvents = 1
		t.ReputationPoints = NeutralEventModifier
		t.ReputationScore = 0.0
		createErr := t.Create(DBStruct)

		if createErr != nil {
			return createErr
		}

		return nil
	}

	result := DBStruct.GetDB().Model(&UserReputation{}).Where("user_id = ? AND guild_id = ?", t.UserID, t.GuildID).Updates(map[string]interface{}{
		"neutral_events":    getResult.NeutralEvents + 1,
		"reputation_points": getResult.ReputationPoints + NeutralEventModifier,
		"reputation_score":  calculateReputationScore(getResult.ReputationPoints+NeutralEventModifier, getResult.PositiveEvents),
	})

	if result.Error != nil {
		modelError := utils.NewModelError(result.Error)
		modelError.SetMessage("Unable to update user reputation for neutral event in DB")
		modelError.SetStatus(http.StatusBadRequest)
		return modelError
	}

	getResultAfterUpdate, getErrAfterUpdate := t.GetByGuild(DBStruct)

	if getErrAfterUpdate != nil {
		return getErr
	}

	*t = *getResultAfterUpdate

	return nil
}

func calculateReputationScore(reputationPoints float64, positiveEvents int) float64 {
	if positiveEvents == 0 {
		return 0.0
	}

	if reputationPoints == 0.0 {
		return 0.0
	}

	var score float64

	score = reputationPoints / float64(positiveEvents)

	return math.Floor(score*100) / 100
}

package models

import (
	"net/http"
	"time"

	"github.com/jinzhu/gorm"
	"gitlab.com/BIC_Dev/reputation-service/utils"
	"gitlab.com/BIC_Dev/reputation-service/utils/db"
)

// ReputationMessageEvent struct
type ReputationMessageEvent struct {
	ID        uint       `gorm:"primary_key" json:"id"`
	UserID    int64      `json:"user_id"`
	GuildID   int64      `json:"guild_id"`
	MessageID int64      `json:"message_id"`
	Message   string     `json:"message"`
	Sentiment int        `json:"sentiment"`
	CreatedAt time.Time  `json:"-"`
	UpdatedAt time.Time  `json:"-"`
	DeletedAt *time.Time `sql:"index" json:"-"`
}

// TableName for the db
func (ReputationMessageEvent) TableName() string {
	return "reputation_message_events"
}

// Create adds a record to DB
func (t *ReputationMessageEvent) Create(DBStruct db.Interface) *utils.ModelError {
	result := DBStruct.GetDB().Create(&t)

	if result.Error != nil {
		modelError := utils.NewModelError(result.Error)
		modelError.SetMessage("Unable to create reputation event entry in DB")
		modelError.SetStatus(http.StatusBadRequest)
		return modelError
	}

	return nil
}

// GetMessage gets the latest record for a reputation event
func (t *ReputationMessageEvent) GetMessage(DBStruct db.Interface) (*ReputationMessageEvent, *utils.ModelError) {
	var event ReputationMessageEvent

	result := DBStruct.GetDB().Model(&ReputationMessageEvent{}).Where("message_id = ?", t.MessageID).Last(&event)

	if gorm.IsRecordNotFoundError(result.Error) {
		return nil, nil
	}

	if result.Error != nil {
		modelError := utils.NewModelError(result.Error)
		modelError.SetMessage("DB error finding message reputation events")
		modelError.SetStatus(http.StatusBadRequest)
		return nil, modelError
	}

	return &event, nil
}

// GetMessageReputationByUser gets the latest record for a reputation event
func (t *ReputationMessageEvent) GetMessageReputationByUser(DBStruct db.Interface) (*ReputationMessageEvent, *utils.ModelError) {
	var event ReputationMessageEvent

	result := DBStruct.GetDB().Model(&ReputationMessageEvent{}).Where("message_id = ? AND user_id = ?", t.MessageID, t.UserID).Last(&event)

	if gorm.IsRecordNotFoundError(result.Error) {
		return nil, nil
	}

	if result.Error != nil {
		modelError := utils.NewModelError(result.Error)
		modelError.SetMessage("DB error finding message reputation events by user")
		modelError.SetStatus(http.StatusBadRequest)
		return nil, modelError
	}

	return &event, nil
}

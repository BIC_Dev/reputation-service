package models

import (
	"net/http"
	"time"

	"gitlab.com/BIC_Dev/reputation-service/utils"
	"gitlab.com/BIC_Dev/reputation-service/utils/db"
)

// PointRewardEvent struct
type PointRewardEvent struct {
	ID        uint       `gorm:"primary_key" json:"id"`
	Reason    string     `json:"reason"`
	UserID    int64      `json:"user_id"`
	GuildID   int64      `json:"guild_id"`
	Points    int        `json:"points"`
	CreatedAt time.Time  `json:"-"`
	UpdatedAt time.Time  `json:"-"`
	DeletedAt *time.Time `sql:"index" json:"-"`
}

// TableName for the db
func (PointRewardEvent) TableName() string {
	return "point_reward_events"
}

// Create adds a record to DB
func (t *PointRewardEvent) Create(DBStruct db.Interface) *utils.ModelError {
	result := DBStruct.GetDB().Create(&t)

	if result.Error != nil {
		modelError := utils.NewModelError(result.Error)
		modelError.SetMessage("Unable to create point reward event entry in DB")
		modelError.SetStatus(http.StatusBadRequest)
		return modelError
	}

	return nil
}

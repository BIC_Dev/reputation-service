package models

import (
	"net/http"
	"time"

	"github.com/jinzhu/gorm"
	"gitlab.com/BIC_Dev/reputation-service/utils"
	"gitlab.com/BIC_Dev/reputation-service/utils/db"
)

// UserPoints struct
type UserPoints struct {
	ID            uint       `gorm:"primary_key" json:"id"`
	UserID        int64      `json:"user_id"`
	GuildID       int64      `json:"guild_id"`
	CurrentPoints int        `json:"current_points"`
	TotalPoints   int        `json:"total_points"`
	CreatedAt     time.Time  `json:"-"`
	UpdatedAt     time.Time  `json:"-"`
	DeletedAt     *time.Time `sql:"index" json:"-"`
}

// TableName for the db
func (UserPoints) TableName() string {
	return "user_points"
}

// Create adds a record to DB
func (t *UserPoints) Create(DBStruct db.Interface) *utils.ModelError {
	result := DBStruct.GetDB().Create(&t)

	if result.Error != nil {
		modelError := utils.NewModelError(result.Error)
		modelError.SetMessage("Unable to create user points entry in DB")
		modelError.SetStatus(http.StatusBadRequest)
		return modelError
	}

	return nil
}

// GetAll gets all point sources for a user
func (t *UserPoints) GetAll(DBStruct db.Interface) ([]*UserPoints, *utils.ModelError) {
	var points []*UserPoints

	result := DBStruct.GetDB().Model(&UserPoints{}).Where("user_id = ?", t.UserID).Find(&points)

	if gorm.IsRecordNotFoundError(result.Error) {
		return points, nil
	}

	if result.Error != nil {
		modelError := utils.NewModelError(result.Error)
		modelError.SetMessage("DB error finding user points for all guilds")
		modelError.SetStatus(http.StatusBadRequest)
		return nil, modelError
	}

	return points, nil
}

// GetByGuild gets a users points by guild
func (t *UserPoints) GetByGuild(DBStruct db.Interface) (*UserPoints, *utils.ModelError) {
	var points UserPoints

	result := DBStruct.GetDB().Model(&UserPoints{}).Where("user_id = ? AND guild_id = ?", t.UserID, t.GuildID).Last(&points)

	if gorm.IsRecordNotFoundError(result.Error) {
		return nil, nil
	}

	if result.Error != nil {
		modelError := utils.NewModelError(result.Error)
		modelError.SetMessage("DB error finding user points by guild")
		modelError.SetStatus(http.StatusBadRequest)
		return nil, modelError
	}

	return &points, nil
}

// UpdatePoints updates a user's points total for a guild
func (t *UserPoints) UpdatePoints(DBStruct db.Interface, points int) *utils.ModelError {
	getResult, getErr := t.GetByGuild(DBStruct)

	if getErr != nil {
		return getErr
	}

	if getResult == nil {
		t.CurrentPoints = points
		t.TotalPoints = points
		createErr := t.Create(DBStruct)

		if createErr != nil {
			return createErr
		}

		return nil
	}

	totalPoints := getResult.TotalPoints

	if points > 0 {
		totalPoints += points
	}

	result := DBStruct.GetDB().Model(&UserPoints{}).Where("user_id = ? AND guild_id = ?", t.UserID, t.GuildID).Updates(map[string]interface{}{
		"current_points": getResult.CurrentPoints + points,
		"total_points":   totalPoints,
	})

	if result.Error != nil {
		modelError := utils.NewModelError(result.Error)
		modelError.SetMessage("Unable to update user points in DB")
		modelError.SetStatus(http.StatusBadRequest)
		return modelError
	}

	getResultAfterUpdate, getErrAfterUpdate := t.GetByGuild(DBStruct)

	if getErrAfterUpdate != nil {
		return getErrAfterUpdate
	}

	*t = *getResultAfterUpdate

	return nil
}

package models

import (
	"net/http"
	"time"

	"gitlab.com/BIC_Dev/reputation-service/utils"
	"gitlab.com/BIC_Dev/reputation-service/utils/db"
)

// PointPurchaseEvent struct
type PointPurchaseEvent struct {
	ID        uint       `gorm:"primary_key" json:"id"`
	Reason    string     `json:"reason"`
	UserID    int64      `json:"user_id"`
	GuildID   int64      `json:"guild_id"`
	Points    int        `json:"points"`
	CreatedAt time.Time  `json:"-"`
	UpdatedAt time.Time  `json:"-"`
	DeletedAt *time.Time `sql:"index" json:"-"`
}

// TableName for the db
func (PointPurchaseEvent) TableName() string {
	return "point_purchase_events"
}

// Create adds a record to DB
func (t *PointPurchaseEvent) Create(DBStruct db.Interface) *utils.ModelError {
	result := DBStruct.GetDB().Create(&t)

	if result.Error != nil {
		modelError := utils.NewModelError(result.Error)
		modelError.SetMessage("Unable to create point purchase event entry in DB")
		modelError.SetStatus(http.StatusBadRequest)
		return modelError
	}

	return nil
}

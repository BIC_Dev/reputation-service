module gitlab.com/BIC_Dev/reputation-service

go 1.14

require (
	github.com/gorilla/mux v1.7.4
	gopkg.in/yaml.v2 v2.3.0
)

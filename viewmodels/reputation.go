package viewmodels

import (
	"net/http"

	"github.com/go-playground/validator"
	"gitlab.com/BIC_Dev/reputation-service/models"
	"gitlab.com/BIC_Dev/reputation-service/utils"
)

// AllGuildUserReputationResponse struct
type AllGuildUserReputationResponse struct {
	Reputations []*models.UserReputation
}

// GuildUserReputationResponse struct
type GuildUserReputationResponse struct {
	Reputation *models.UserReputation
}

// UpdateReputationRequest struct
type UpdateReputationRequest struct {
	Action      string  `json:"action" validate:"required"`
	ActorID     int64   `json:"actor_id" validate:"required"`
	GuildID     int64   `json:"guild_id" validate:"required"`
	Message     string  `json:"message"`
	MessageID   int64   `json:"message_id" validate:"required"`
	TaggedUsers []int64 `json:"tagged_users"`
}

// UpdateReputationResponse struct
type UpdateReputationResponse struct {
	Message                 string                           `json:"message"`
	ReputationMessageEvents []*models.ReputationMessageEvent `json:"reputation_message_events"`
	ReputationTagEvents     []*models.ReputationTagEvent     `json:"reputation_tag_events"`
	UserReputations         []*models.UserReputation         `json:"user_reputations"`
	PointReputationEvents   []*models.PointReputationEvent   `json:"point_reputation_events"`
	UserPoints              []*models.UserPoints             `json:"user_points"`
}

// Validate validates the create user request params
func (r *UpdateReputationRequest) Validate() *utils.ValidationError {
	var validate *validator.Validate
	validate = validator.New()

	if err := validate.Struct(r); err != nil {
		validationError := utils.NewValidationError(err)
		validationError.SetStatus(http.StatusBadRequest)
		validationError.SetMessage("Validation failed on request to update reputation")

		return validationError
	}

	return nil
}

package viewmodels

import (
	"net/http"

	"github.com/go-playground/validator"
	"gitlab.com/BIC_Dev/reputation-service/models"
	"gitlab.com/BIC_Dev/reputation-service/utils"
)

// AllGuildUserPointsResponse struct
type AllGuildUserPointsResponse struct {
	AllPoints []*models.UserPoints `json:"all_points"`
}

// GuildUserPointsResponse struct
type GuildUserPointsResponse struct {
	Points *models.UserPoints `json:"points"`
}

// UpdatePointsRequest struct
type UpdatePointsRequest struct {
	Action      string `json:"action" validate:"required"`
	Reason      string `json:"reason" validate:"required"`
	UserID      int64  `json:"user_id" validate:"required"`
	GuildID     int64  `json:"guild_id" validate:"required"`
	AdminUserID int64  `json:"admin_user_id"`
	Points      int    `json:"points" validate:"required"`
}

// UpdatePointsResponse struct
type UpdatePointsResponse struct {
	Message              string                       `json:"message"`
	PointPurchaseEvent   *models.PointPurchaseEvent   `json:"point_purchase_event"`
	PointRewardEvents    *models.PointRewardEvent     `json:"point_reward_event"`
	PointAdminEvent      *models.PointAdminEvent      `json:"point_admin_event"`
	PointReputationEvent *models.PointReputationEvent `json:"point_reputation_event"`
	PointRefundEvent     *models.PointRefundEvent     `json:"point_refund_event"`
	UserPoints           *models.UserPoints           `json:"user_points"`
}

// Validate validates the create user request params
func (r *UpdatePointsRequest) Validate() *utils.ValidationError {
	var validate *validator.Validate
	validate = validator.New()

	if err := validate.Struct(r); err != nil {
		validationError := utils.NewValidationError(err)
		validationError.SetStatus(http.StatusBadRequest)
		validationError.SetMessage("Validation failed on request to update points")

		return validationError
	}

	return nil
}

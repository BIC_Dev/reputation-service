package routes

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/BIC_Dev/reputation-service/controllers"
)

// GetRouter creates and returns a router
func GetRouter() *mux.Router {
	return mux.NewRouter().StrictSlash(true)
}

// AddRoutes adds all necessary routes to the router
func AddRoutes(router *mux.Router, c *controllers.Controller) {
	router.HandleFunc("/reputation-service/status", c.GetStatus).Methods("GET")

	router.HandleFunc("/reputation-service/reputation/{user_id}", c.GetAllReputation).Methods("GET")
	router.HandleFunc("/reputation-service/reputation/{user_id}/{guild_id}", c.GetReputation).Methods("GET")
	router.HandleFunc("/reputation-service/reputation", c.UpdateReputation).Methods("PUT")

	router.HandleFunc("/reputation-service/points/{user_id}", c.GetAllPoints).Methods("GET")
	router.HandleFunc("/reputation-service/points/{user_id}/{guild_id}", c.GetPoints).Methods("GET")
	router.HandleFunc("/reputation-service/points", c.UpdatePoints).Methods("PUT")
}

// StartListener starts the HTTP listener
func StartListener(router *mux.Router, port string) {
	listenerPort := fmt.Sprintf(":%s", port)
	log.Fatal(http.ListenAndServe(listenerPort, router))
}
